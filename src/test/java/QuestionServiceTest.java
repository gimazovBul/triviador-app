import org.junit.Before;
import org.junit.Test;
import ru.itis.dto.BaseDto;
import ru.itis.dto.BaseQuestionDto;
import ru.itis.dto.QuestionDto;
import ru.itis.dto.RoomDto;
import ru.itis.serverConfig.ServerContext;
import ru.itis.serverServices.interfaces.QuestionService;
import ru.itis.serverSideWrappers.RoomWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class QuestionServiceTest {
    private ServerContext serverContext;
    private QuestionService questionService;
    private RoomDto roomDto;

    @Before
    public void init() {
        roomDto = new RoomDto();
        serverContext = ServerContext.get();
        questionService = serverContext.getQuestionService();
    }

    @Test
    public void shouldReturnMapWithQuestion() {
        RoomWrapper.set(roomDto);
        assertNotNull(questionService.prepareQuestion());
    }

    @Test
    public void shouldNotReturnQuestionFromOldQuestions(){
        List<BaseQuestionDto> questions = new ArrayList<>();
        QuestionDto question1 = new QuestionDto(0);
        question1.setId("1");
        question1.setText("Which programming language doesn't exist?");
        question1.setAnswers(new String[]{"C#", "JS", "Python"});

        QuestionDto question2 = new QuestionDto(0);
        question2.setId("2");
        question2.setText("Which programming language is the best?");
        question2.setAnswers(new String[]{"Java", "Russian", "Maven"});

        QuestionDto question3 = new QuestionDto(1);
        question3.setId("3");
        question3.setText("Which country won't exist in nearest future?");
        question3.setAnswers(new String[]{"Israel", "Ukraine", "Sun"});

        questions.add(question1);
        questions.add(question2);
        questions.add(question3);

        roomDto.setQuestions(questions);
        RoomWrapper.set(roomDto);
        Map<String, BaseDto> newQuestion = questionService.prepareQuestion();
        QuestionDto question = (QuestionDto) newQuestion.get("quesResp");

        assertFalse(questions.contains(question));
    }
}