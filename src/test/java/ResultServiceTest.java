
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.itis.dto.*;
import ru.itis.serverConfig.ServerContext;
import ru.itis.serverServices.interfaces.ResultService;
import ru.itis.serverSideWrappers.RoomWrapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ResultServiceTest {
    private RoomDto roomDto;
    private ServerContext serverContext;
    private ResultService resultService;

    @Before
    public void init() {
        roomDto = new RoomDto();
        serverContext = ServerContext.get();
        resultService = serverContext.getResultService();
    }

    @Test
    public void shouldSet2PointsToUser2And0ToOthers(){
        UserDto userDto1 = UserDto.builder()
                .pointsAmount(0)
                .id("1")
                .build();
        UserDto userDto2 = UserDto.builder()
                .pointsAmount(0)
                .id("2")
                .build();
        UserDto userDto3 = UserDto.builder()
                .pointsAmount(0)
                .id("3")
                .build();

        List<AnswerDto> answersArr = Arrays.asList(
                new AnswerDto("1", -1),
                new AnswerDto("2", 0),
                new AnswerDto("3", 1)
        );
        List<AnswerDto> answers = new ArrayList<>(answersArr);

        QuestionDto question = new QuestionDto();
        question.setRightInd(0);
        question.setId("1");
        question.setText("Which programming language doesn't exist?");
        question.setAnswers(new String[]{"C#", "JS", "Python"});
        List<BaseQuestionDto> questions = Collections.singletonList(question);

        List<UserDto> users = new ArrayList<>();
        users.add(userDto1);
        users.add(userDto2);
        users.add(userDto3);

        roomDto.setPlayers(users);
        roomDto.setQuestions(questions);
        RoomWrapper.set(roomDto);
        ResultDto result = (ResultDto) resultService.calculate(answers).get("res");
        Assert.assertEquals(2, result.getUsers().get(1).getPointsAmount());

    }
    @Test
    public void shouldSet2PointsToUser1And1ToOthers(){
        UserDto userDto1 = UserDto.builder()
                .pointsAmount(0)
                .id("1")
                .build();
        UserDto userDto2 = UserDto.builder()
                .pointsAmount(0)
                .id("2")
                .build();
        UserDto userDto3 = UserDto.builder()
                .pointsAmount(0)
                .id("3")
                .build();

        List<AnswerDto> answersArr = Arrays.asList(
                new AnswerDto("1", 0),
                new AnswerDto("2", 0),
                new AnswerDto("3", 0)
        );
        List<AnswerDto> answers = new ArrayList<>(answersArr);

        QuestionDto question = new QuestionDto();
        question.setRightInd(0);
        question.setId("1");
        question.setText("Which programming language doesn't exist?");
        question.setAnswers(new String[]{"C#", "JS", "Python"});
        List<BaseQuestionDto> questions = Collections.singletonList(question);

        List<UserDto> users = new ArrayList<>();
        users.add(userDto1);
        users.add(userDto2);
        users.add(userDto3);

        roomDto.setPlayers(users);
        roomDto.setQuestions(questions);
        RoomWrapper.set(roomDto);
        ResultDto result = (ResultDto) resultService.calculate(answers).get("res");
        Assert.assertEquals(2, result.getUsers().get(0).getPointsAmount());
        Assert.assertEquals(1, result.getUsers().get(1).getPointsAmount());
        Assert.assertEquals(1, result.getUsers().get(2).getPointsAmount());

    }
}