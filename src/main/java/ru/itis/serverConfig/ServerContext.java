package ru.itis.serverConfig;



import lombok.Data;
import ru.itis.dto.BaseQuestionDto;
import ru.itis.dto.QuestionDto;
import ru.itis.serverServices.impl.QuestionServiceImpl;
import ru.itis.serverServices.impl.ResultServiceImpl;
import ru.itis.serverServices.interfaces.QuestionService;
import ru.itis.serverServices.interfaces.ResultService;

import java.util.Arrays;
import java.util.List;

@Data
public class ServerContext {
    private static ServerContext serverContext;
    private QuestionService questionService;
    private List<BaseQuestionDto> questions;
    private ResultService resultService;

    public static ServerContext get() {
        if (serverContext == null) {
            serverContext = new ServerContext();
        }
        return serverContext;
    }

    private ServerContext() {
        questions = createQuestions();
        questionService = new QuestionServiceImpl();
        resultService = new ResultServiceImpl();
    }

    private List<BaseQuestionDto> createQuestions() {
        QuestionDto question1 = new QuestionDto(0);
        question1.setId("1");
        question1.setText("Which programming language doesn't exist?");
        question1.setAnswers(new String[]{"C#", "JS", "Python"});

        QuestionDto question2 = new QuestionDto(0);
        question2.setId("2");
        question2.setText("Which programming language is the best?");
        question2.setAnswers(new String[]{"Java", "Russian", "Maven"});

        QuestionDto question3 = new QuestionDto(1);
        question3.setId("3");
        question3.setText("Which country won't exist in nearest future?");
        question3.setAnswers(new String[]{"Israel", "Ukraine", "Sun"});

        QuestionDto question4 = new QuestionDto(2);
        question4.setId("4");
        question4.setText("Please continue... ноль, целковый,..");
        question4.setAnswers(new String[]{"ноль", "Четвертушка", "Полушка"});

        QuestionDto question5 = new QuestionDto(1);
        question5.setId("5");
        question5.setText("Best country?");
        question5.setAnswers(new String[]{"USA", "Russia", "Israel"});

        QuestionDto question6 = new QuestionDto(1);
        question6.setId("6");
        question6.setText("Did Epstein kill himself");
        question6.setAnswers(new String[]{"Yes", "No", "Where am i"});

        QuestionDto question7 = new QuestionDto(2);
                question7.setId("7");
                question7.setText("Понравилось приложение?????");
                question7.setAnswers(new String[]{"Как выйти", "Атстой", "Да, круто!"});

        QuestionDto question8 = new QuestionDto(0);
        question8.setId("8");
        question8.setText("?");
        question8.setAnswers(new String[]{"!", "?", "..."});

        return Arrays.asList(
                question1,
                question2,
                question3,
                question4,
                question5,
                question6,
                question7,
                question8
        );
    }
}
