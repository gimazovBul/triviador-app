package ru.itis.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = CodeDto.class, name = "codeDto"),
        @JsonSubTypes.Type(value = RoomDto.class, name = "roomDto"),
        @JsonSubTypes.Type(value = FailDto.class, name = "failDto"),
        @JsonSubTypes.Type(value = BaseQuestionDto.class, name = "baseQuestionDto"),
        @JsonSubTypes.Type(value = QuestionDto.class, name = "questionDto"),
        @JsonSubTypes.Type(value = AnswerDto.class, name = "answerDto"),
        @JsonSubTypes.Type(value = QuestionRequestDto.class, name = "questionRequestDto"),

})
public class BaseDto {
}
