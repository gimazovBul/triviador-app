package ru.itis.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import  ru.itis.dto.BaseQuestionDto;
import  ru.itis.dto.UserDto;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoomDto extends BaseDto {
    private String code;
    private Map<String, Boolean> colors; //true if color already taken
    private final int AMOUNT = 3; //amount of players
    private List<UserDto> players;
    private List<BaseQuestionDto> questions; //list of answered questions

    public RoomDto(String code) {
        colors = new ConcurrentHashMap<>();
        colors.put("#0a9c42", false);
        colors.put("#0000FF", false);
        colors.put("#c2c200", false);
        colors.put("#8E169C", false);
        colors.put("#ff4ce4", false);
        colors.put("#415508", false);
        colors.put("#007681", false);
        colors.put("#7b3b30", false);
        players = new CopyOnWriteArrayList<>();
        this.code = code;
    }
}
