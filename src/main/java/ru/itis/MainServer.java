package ru.itis;

import ru.itis.config.ServerStarter;

public class MainServer {
    public static void main(String[] args) {
        System.out.println("server started...");
        new ServerStarter(args);
    }
}
