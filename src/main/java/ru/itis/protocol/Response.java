package ru.itis.protocol;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Response<T> {

    @JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, property="@class")
    private T data;
    private String header;

    public T getData(){
        return data;
    }

    public Response(T data, String header){
        this.data = data;
        this.header = header;
    }

    public static <E> Response<E> build(E data, String header) {
        return new Response<>(data, header);
    }}
