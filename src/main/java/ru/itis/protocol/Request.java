package ru.itis.protocol;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Request<T>{

    @JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, property="@class")
    private T data;
    private String header;

    public T getData(){
        return data;
    }

    //поменять местами
    public Request(String header, T data){
        this.header = header;
        this.data = data;
    }

    public String getHeader() {
        return header;
    }

    public static <E> Request<E> build(String header, E data) {
        return new Request<>(header, data);
    }
}
