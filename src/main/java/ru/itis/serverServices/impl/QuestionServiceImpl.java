package ru.itis.serverServices.impl;



import ru.itis.dto.BaseDto;
import ru.itis.dto.BaseQuestionDto;
import ru.itis.dto.RoomDto;
import ru.itis.serverServices.interfaces.QuestionService;
import ru.itis.serverSideWrappers.RoomWrapper;

import java.util.*;

import static ru.itis.serverConfig.ServerContext.get;

public class QuestionServiceImpl implements QuestionService {

    @Override
    public Map<String, BaseDto> prepareQuestion() {
        Random random = new Random();
        Map<String, BaseDto> map = new HashMap<>();
        List<BaseQuestionDto> questions = new ArrayList<>(get().getQuestions());
        RoomDto roomDto = RoomWrapper.get();
        List<BaseQuestionDto> oldQuestions = roomDto.getQuestions();
        if (oldQuestions != null) oldQuestions = new ArrayList<>(roomDto.getQuestions());
        if (oldQuestions == null || (questions.removeAll(oldQuestions) && questions.size() != 0)) {
            BaseQuestionDto dto = questions.get(random.nextInt(questions.size()));
            if (oldQuestions == null)
                oldQuestions = new ArrayList<>();
            oldQuestions.add(dto); //oldQuestions = previous questions + this last one
            roomDto.setQuestions(oldQuestions);
            System.out.println("oldQuestions " + oldQuestions);
            map.put("quesResp", dto);
            return map;
        }
        return null;
    }
}
