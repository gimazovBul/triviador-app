package ru.itis.serverServices.impl;


import ru.itis.dto.*;
import ru.itis.serverServices.interfaces.ResultService;
import ru.itis.serverSideWrappers.RoomWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.itis.serverConfig.ServerContext.get;


public class ResultServiceImpl implements ResultService {

    @Override
    public Map<String, BaseDto> calculate(List<AnswerDto> answers) {
        System.out.println("calculating results...");
        boolean setMaxPoint = false; //true if someone got max points
        RoomDto roomDto = RoomWrapper.get();
        List<UserDto> users = new ArrayList<>();
        for (AnswerDto answer : answers) {
            for (UserDto player : roomDto.getPlayers()) {
                if (player.getId().equals(answer.getUserId())) {
                    //that long thing means last question
                    if (answer.getAnswerInd() == roomDto.getQuestions().get(roomDto.getQuestions().size() - 1).getRightInd()) {
                        if (answers.indexOf(answer) == 0 || answers.get(0).getAnswerInd() !=
                                roomDto.getQuestions().get(roomDto.getQuestions().size() - 1).getRightInd() && !setMaxPoint){
                            player.setPointsAmount(player.getPointsAmount() + roomDto.getQuestions().get(roomDto.getQuestions().size() - 1).getMax());
                            setMaxPoint = true;
                        }
                        else
                            player.setPointsAmount(player.getPointsAmount() + roomDto.getQuestions().get(roomDto.getQuestions().size() - 1).getMax() - 1);
                    }
                    users.add(player);
                }
            }
        }
        answers.clear();
        Map<String, BaseDto> resMap = new HashMap<>();
        if (roomDto.getQuestions().size() == get().getQuestions().size())
            resMap.put("res", new ResultDto(ResultDto.Type.END, users));
        else resMap.put("res", new ResultDto(ResultDto.Type.CONT, users));

        return resMap;
    }
}
