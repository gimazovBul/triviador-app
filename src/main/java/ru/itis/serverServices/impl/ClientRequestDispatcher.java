package ru.itis.serverServices.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import ru.itis.dto.BaseDto;
import ru.itis.dto.CodeDto;
import ru.itis.protocol.Request;
import ru.itis.server.ClientHandler;
import ru.itis.serverServices.interfaces.RequestDispatcher;
import ru.itis.serverSideWrappers.UserWrapper;

import java.util.Map;

@NoArgsConstructor
public class ClientRequestDispatcher implements RequestDispatcher {
    private ObjectMapper mapper;
    private ClientHandler clientHandler;

    public ClientRequestDispatcher(ClientHandler clientHandler) {
        mapper = new ObjectMapper();
        this.clientHandler = clientHandler;
    }

    @Override
    public Map<String, BaseDto> resolve(String message) throws JsonProcessingException {
        Request request = mapper.readValue(message, Request.class);
        Map<String, BaseDto> resMap = null;
        System.out.println("UserDto " + UserWrapper.get());
        switch (request.getHeader()) {
            case "code": {
                CodeDto codeDto = (CodeDto) request.getData();
                System.out.println(codeDto);
                clientHandler.setRoomHandler(clientHandler.getServer().getDefaultRoom());
                resMap = clientHandler.tryJoinTheRoom(codeDto.getCode(), codeDto.getUserId());
                break;
            }
            case "quesReq":
            case "answer": {
                System.out.println("ClientRequestDispatcher: sending mess to RoomHandler");
                clientHandler.getRoomHandler().resolve(message);
                break;
            }
        }
        if (resMap != null) {
            System.out.println("sending dto to ClientHandler");
            return resMap;
        } else return null;
    }
}
