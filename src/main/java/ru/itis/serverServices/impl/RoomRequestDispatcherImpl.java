package ru.itis.serverServices.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.itis.dto.AnswerDto;
import ru.itis.dto.BaseDto;
import ru.itis.protocol.Request;
import ru.itis.server.RoomHandler;
import ru.itis.serverConfig.ServerContext;
import ru.itis.serverServices.interfaces.QuestionService;
import ru.itis.serverServices.interfaces.RequestDispatcher;
import ru.itis.serverServices.interfaces.ResultService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RoomRequestDispatcherImpl implements RequestDispatcher {
    private ObjectMapper mapper;
    private QuestionService questionService;
    private RoomHandler roomHandler;
    private ResultService resultService;
    private List<AnswerDto> answers;

    public RoomRequestDispatcherImpl(RoomHandler roomHandler) {
        ServerContext serverContext = ServerContext.get();
        mapper = new ObjectMapper();
        questionService = serverContext.getQuestionService();
        resultService = serverContext.getResultService();
        this.roomHandler = roomHandler;
        answers = new ArrayList<>();
    }

    @Override
    public Map<String, BaseDto> resolve(String message) throws JsonProcessingException {
        Request request = mapper.readValue(message, Request.class);
        Map<String, BaseDto> resMap = null;

        switch (request.getHeader()) {
            case "quesReq": {
                resMap = questionService.prepareQuestion();
                break;
            }
            case "answer": {
                AnswerDto answerDto = (AnswerDto) request.getData();
                answers.add(answerDto);
                if (answers.size() == roomHandler.getRoomDto().getAMOUNT())
                    resMap = resultService.calculate(answers);
                System.out.println("answers " + answers);
                break;
            }
        }
        System.out.println("sending dto to RoomHandler " + resMap);
        return resMap;
    }
}
