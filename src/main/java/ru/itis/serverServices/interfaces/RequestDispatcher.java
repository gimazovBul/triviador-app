package ru.itis.serverServices.interfaces;

import com.fasterxml.jackson.core.JsonProcessingException;
import ru.itis.dto.BaseDto;

import java.util.Map;

public interface RequestDispatcher {
    Map<String, BaseDto> resolve(String message) throws JsonProcessingException;
}
