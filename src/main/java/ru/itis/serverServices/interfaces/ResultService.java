package ru.itis.serverServices.interfaces;

import ru.itis.dto.AnswerDto;
import ru.itis.dto.BaseDto;

import java.util.List;
import java.util.Map;

public interface ResultService {
    Map<String, BaseDto> calculate(List<AnswerDto> answers);
}
