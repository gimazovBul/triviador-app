package ru.itis.serverServices.interfaces;


import ru.itis.dto.BaseDto;

import java.util.Map;

public interface QuestionService {
   Map<String, BaseDto> prepareQuestion();
}
