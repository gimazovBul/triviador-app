package ru.itis.server;


import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;
import ru.itis.dto.BaseDto;
import ru.itis.dto.UserDto;
import ru.itis.serverServices.impl.ClientRequestDispatcher;
import ru.itis.serverServices.interfaces.RequestDispatcher;
import ru.itis.serverSideWrappers.UserWrapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Map;

@Data
public class ClientHandler extends Thread {
    private Socket clientSocket;
    private RequestDispatcher clientRequestDispatcher;
    private RoomHandler roomHandler;
    private ObjectMapper mapper;
    private Server server;

    public ClientHandler(Socket client, Server server) {
        mapper = new ObjectMapper();
        this.clientSocket = client;
        this.server = server;
        clientRequestDispatcher = new ClientRequestDispatcher(this);
    }

    public Map<String, BaseDto> tryJoinTheRoom(String code, String userId){
        return roomHandler.addToRoom(this, code, userId);
    }

    @Override
    public String toString() {
        return "ClientHandler{}";
    }

    @Override
    public void run() {
        try {
            UserWrapper.set(new UserDto());
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));
            String input;
            while ((input = in.readLine()) != null) {
                Map<String, BaseDto> resMap = clientRequestDispatcher.resolve(input);
                if (resMap != null) {
                    Map.Entry<String, BaseDto> entry = resMap.entrySet().stream().findFirst().get();
                    roomHandler.sendMessage(entry.getValue(), entry.getKey(), this);
                }
            }
        }
        catch (IOException e) {
            throw new IllegalStateException(e);
        }

        //todo если кто-то отвалился, то ...

    }
}
