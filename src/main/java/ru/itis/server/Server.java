package ru.itis.server;


import lombok.Data;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;


@Data
public class Server {

    private List<ClientHandler> clients;
    private Map<String, RoomHandler> rooms;
    private RoomHandler defaultRoom;

    public Server() {
        clients = new CopyOnWriteArrayList<>();
        rooms = new ConcurrentHashMap<>();
        defaultRoom = new RoomHandler();
        rooms.put("", defaultRoom);
    }

    public void start(int port) {
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        while (true) {
            try {
                ClientHandler clientHandler = new ClientHandler(serverSocket.accept(), this);
                clientHandler.start();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

}
