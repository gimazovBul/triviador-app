package ru.itis.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import ru.itis.dto.BaseDto;
import ru.itis.dto.FailDto;
import ru.itis.dto.RoomDto;
import ru.itis.dto.UserDto;
import ru.itis.protocol.Response;
import ru.itis.serverServices.impl.RoomRequestDispatcherImpl;
import ru.itis.serverServices.interfaces.RequestDispatcher;
import ru.itis.serverSideWrappers.RoomWrapper;
import ru.itis.serverSideWrappers.UserWrapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@Data
public class RoomHandler {
    private RequestDispatcher requestDispatcher;
    private List<ClientHandler> clientHandlers;
    private List<BufferedReader> inList;
    private List<PrintWriter> outList;
    private RoomDto roomDto;
    private RequestDispatcher roomRequestDispatcher;
    private ObjectMapper mapper;

    public RoomDto getRoomDto() {
        return roomDto;
    }

    public List<ClientHandler> getClientHandlers() {
        return clientHandlers;
    }

    public RoomHandler(String code) {
        clientHandlers = new CopyOnWriteArrayList<>();
        roomDto = new RoomDto(code);
        inList = new CopyOnWriteArrayList<>();
        outList = new CopyOnWriteArrayList<>();
        mapper = new ObjectMapper();
        roomRequestDispatcher = new RoomRequestDispatcherImpl(this);
    }

    public RoomHandler(){
        clientHandlers = new CopyOnWriteArrayList<>();
        mapper = new ObjectMapper();
        roomRequestDispatcher = new RoomRequestDispatcherImpl(this);
        inList = new CopyOnWriteArrayList<>();
        outList = new CopyOnWriteArrayList<>();
    }

    public synchronized Map<String, BaseDto> addToRoom(ClientHandler client, String code, String userId) {
        Map<String, BaseDto> resMap = new HashMap<>();
        boolean exists = false;
        if (client.getServer().getRooms().get(code) != null) {
            exists = true;
            RoomHandler room = client.getServer().getRooms().get(code);
            for (UserDto u : room.getRoomDto().getPlayers()) {
                if (u.getId().equals(userId)) throw new IllegalStateException();
            }
            if (room.getClientHandlers().size() < room.getRoomDto().getAMOUNT()) {
                System.out.println(room.toString() + " exists");
                roomDto = room.setUsersInfo(client, room, userId);
                client.setRoomHandler(room);
                resMap.put("sucJoin", roomDto);
                RoomWrapper.set(roomDto);
                return resMap;
            } else if (room.getClientHandlers().size() == room.getRoomDto().getAMOUNT()) {
                System.out.println("room exists and full...");
                FailDto failDto = new FailDto();
                System.out.println("client got default room...");
                failDto.setMessage("room is full, try other code :)");
                resMap.put("fail", failDto);
                System.out.println("failDto " + failDto);
                return resMap;
            }
        }
        if (!exists) {
            System.out.println("Such room doesn't exist...");
            RoomHandler room = new RoomHandler(code);
            client.getServer().getRooms().put(code, room);
            client.setRoomHandler(room);
            roomDto = room.setUsersInfo(client, room, userId);
            UserWrapper.get().setHost(true);
            resMap.put("room", roomDto);
            RoomWrapper.set(roomDto);
            return resMap;
        }
        throw new IllegalStateException();
    }

    private void addToLists(ClientHandler client) throws IOException {
        clientHandlers.add(client);
        this.getRoomDto().getPlayers().add(UserWrapper.get());
        inList.add(new BufferedReader(
                new InputStreamReader(client.getClientSocket().getInputStream())));
        outList.add(new PrintWriter(client.getClientSocket().getOutputStream(), true));
    }

    private RoomDto setUsersInfo(ClientHandler client, RoomHandler room, String userId) {
        client.getServer().getClients().add(client);
        RoomDto roomDto;
        try {
            room.addToLists(client);
            roomDto = room.getRoomDto();
            UserWrapper.get().setPointsAmount(0);
            List<String> freeColors = room.getRoomDto().getColors().entrySet()
                    .stream()
                    .filter(e -> !e.getValue())
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());
            int colorNum = new Random().nextInt(freeColors.size());
            String color = freeColors.get(colorNum);
            UserWrapper.get().setColor(color);
            UserWrapper.get().setId(userId);
            room.getRoomDto().getColors().put(color, true);
            System.out.println("user created " + UserWrapper.get().toString());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        System.out.println("current room state " + roomDto.toString());
        return roomDto;
    }


    public synchronized void sendMessage(BaseDto responseDto, String header, ClientHandler clientHandler) throws IOException {
        System.out.println("Roomhandler: sendMessage " + Thread.currentThread().getName());
        Response response = Response.build(responseDto, header);
        System.out.println(response);
        if (response.getHeader().equals("fail")) {
            PrintWriter printWriter = new PrintWriter(clientHandler.getClientSocket().getOutputStream(), true);
            printWriter.println(mapper.writeValueAsString(response));
        } else if (roomDto.getPlayers().size() == roomDto.getAMOUNT())
            for (PrintWriter writer : outList) {
                writer.println(mapper.writeValueAsString(response));
            }
    }

    public synchronized void resolve(String mess) {
        try {
            System.out.println("RoomHandler: got mess from ClientHandler");
            Map<String, BaseDto> map = roomRequestDispatcher.resolve(mess);
            if (map != null) {
                Map.Entry<String, BaseDto> entry = map.entrySet().stream().findFirst().get();
                System.out.println("sending message to client...");
                sendMessage(entry.getValue(), entry.getKey(), this.getClientHandlers().get(0));
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public String toString() {
        return "RoomHandler{}";
    }
}
