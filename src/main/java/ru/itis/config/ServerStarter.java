package ru.itis.config;

import com.beust.jcommander.JCommander;
import ru.itis.server.Server;

public class ServerStarter {

    public ServerStarter(String[] args) {
        Args argsServerClass = new Args();
        JCommander.newBuilder()
                .addObject(argsServerClass)
                .args(args)
                .build();
        startServer(argsServerClass.getPort());
    }

    private void startServer(int port){
        Server server = new Server();
        server.start(port);
    }
}
