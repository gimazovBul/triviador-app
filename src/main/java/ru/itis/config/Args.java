package ru.itis.config;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Args {
    @Parameter(names = {"--port"})
    private
    Integer port;
    public Integer getPort() {
        return port;
    }
}
