package ru.itis.serverSideWrappers;


import ru.itis.dto.RoomDto;

public class RoomWrapper {
    private static ThreadLocal<RoomDto> threadLocal = new ThreadLocal<>();

    public static void set(RoomDto roomDto){
        threadLocal.set(roomDto);
    }

    public static RoomDto get(){
        return threadLocal.get();
    }
}
