package ru.itis.serverSideWrappers;

import ru.itis.dto.UserDto;

public class UserWrapper {
    private static final ThreadLocal<UserDto> threadLocal = new ThreadLocal<>();

    public static void set(UserDto user){
        threadLocal.set(user);
    }
    
    public static UserDto get(){
        return threadLocal.get();
    }
}
